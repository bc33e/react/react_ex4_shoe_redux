import React, { Component } from "react";
import { data_shoes } from "./data_shoes";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/shoeConstant";
import ShoeItem from "./ShoeItem";
import ShoeCart from "./ShoeCart";

class Ex_Shoe extends Component {
  state = {
    shoe: data_shoes,
    cart: [],
  };

  renderContent = () => {
    return this.props.listShoe.map((item, index) => {
      return (
        <ShoeItem
          key={index}
          handleAddToCart={this.props.handleAddToCart}
          data={item}
        />
      );
    });
  };

  render() {
    return (
      <div className="container py-5">
        <ShoeCart />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listShoe: state.shoeReducer.shoe,
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Ex_Shoe);

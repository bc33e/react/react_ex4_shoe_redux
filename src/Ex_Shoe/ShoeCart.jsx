import React, { Component } from "react";
import { connect } from "react-redux";
import { handleChangeQuantityAction } from "./redux/actions/quantityAction";

class ShoeCart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.quantity}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(1, item.id);
              }}
              className="btn btn-success"
            >
              +
            </button>
            <span className="px-5">{item.quantity}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(-1, item.id);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead className="bg bg-secondary text-light">
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (quantity, shoeId) => {
      dispatch(handleChangeQuantityAction(quantity, shoeId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoeCart);

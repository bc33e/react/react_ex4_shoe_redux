import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART, CHANGE_QUANTITY } from "../constants/shoeConstant";

let initialState = {
  shoe: data_shoes,
  cart: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.cart.findIndex((item) => item.id == payload.id);
      let cloneCart = [...state.cart];
      if (index == -1) {
        let newShoe = { ...payload, quantity: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].quantity++;
      }

      state.cart = cloneCart;
      return { ...state };
    }

    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.shoeId);
      if (index != -1) {
        cloneCart[index].quantity += payload.quantity;
        cloneCart[index].quantity <= 0 && cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};

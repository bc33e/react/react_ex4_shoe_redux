import { CHANGE_QUANTITY } from "../constants/shoeConstant";
export const handleChangeQuantityAction = (quantity, shoeId) => {
  return {
    type: CHANGE_QUANTITY,
    payload: { quantity, shoeId },
  };
};

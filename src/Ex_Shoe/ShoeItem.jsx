import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 mt-5">
        <div className="card text-white bg-secondary">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-title">{price}</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-info"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
